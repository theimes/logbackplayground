package de.thiss.example.logback.LogbackTutorial.architecture;

//Import SLF4J classes.
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;


public class AppConfiguredWithFile {
	public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(AppConfiguredWithFile.class);
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();

        ClassLoader classLoader = AppConfiguredWithFile.class.getClassLoader();
        // try config 1, 2, 3
        String configFilePath = "architecture/sample-config-6.xml";
        String path = classLoader.getResource(configFilePath).getPath();
        logger.debug(path);

        try {
            JoranConfigurator configurator = new JoranConfigurator();
            lc.reset();
            configurator.setContext(lc);
            configurator.doConfigure(path);
        } catch (JoranException je) {
            StatusPrinter.print(lc.getStatusManager());
        }
        logger.info("Entering application.");
        Bar bar = new Bar();
        bar.doIt();
        logger.info("Exiting application.");

    }
}
