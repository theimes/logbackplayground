package de.thiss.example.logback.LogbackTutorial.architecture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Bar {
    Logger logger = LoggerFactory.getLogger(Bar.class);
    Logger log = LoggerFactory.getLogger("hi");

    public void doIt() {
        logger.info("doing my job");
        log.info("Another log message");
    }
}