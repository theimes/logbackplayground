package de.thiss.example.logback.LogbackTutorial.groovy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;


public class GroovyLogging {
	
	public static void main(String... args) {
		Logger logger = LoggerFactory.getLogger(GroovyLogging.class);
		logger.info("That's groovy man.... behave");
		logger.error("Thou shall not pass");
		logger.error("But who am I? I'm just kidding ;-)");
		
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        StatusPrinter.print(lc);
	}

}
