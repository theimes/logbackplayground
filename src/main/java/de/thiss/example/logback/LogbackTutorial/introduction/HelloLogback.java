package de.thiss.example.logback.LogbackTutorial.introduction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;

public class HelloLogback {

	public static void main(String[] args) {

        Logger logger = LoggerFactory.getLogger(HelloLogback.class);
        logger.debug("Hello loggack world.");
        
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        StatusPrinter.print(lc);
    }
}
