import ch.qos.logback.core.ConsoleAppender
import net.logstash.logback.encoder.LogstashEncoder

import net.logstash.logback.fieldnames.LogstashFieldNames

import static ch.qos.logback.classic.Level.DEBUG
import static ch.qos.logback.classic.Level.INFO

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.filter.ThresholdFilter
import ch.qos.logback.core.ConsoleAppender
import io.sentry.logback.SentryAppender

// Logging to local console
appender("Console", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
  }
}

// Logging formatted for logstash
appender("JSON", ConsoleAppender) {
  encoder(LogstashEncoder) {
	fieldNames(LogstashFieldNames) {
            timestamp = "timestamp"
    }
    customFields = '{"service_name": "salesforce-push-processor"}'
  }
}

// Logging to sentry
appender("Sentry", SentryAppender) {
  filter(ThresholdFilter) {
    level = WARN
  }
}

def appenderList = []

def isLocalLogging = System.getenv("LOGGING_MODE") == "local-console"
def isSentryEnabled = System.getenv("SENTRY_DSN") != null

if (isLocalLogging) {
    appenderList = ["Console"]
} else {
    appenderList = ["JSON"]
}
if (isSentryEnabled) {
    appenderList.add("Sentry")
}

root(INFO, appenderList)